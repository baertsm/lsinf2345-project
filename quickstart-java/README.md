How to run the program?
=======================

First, import the program (`quickstart-java` dir) in Eclipse (or compile Java code with `javac`).


Pre-processing
--------------

First, run `ConnectedComponentsPreProcessing` and use an input file as argument:

        $ java ConnectedComponentsPreProcessing input.txt

It will produce two files: `vertices.txt` and `edges.txt`.


Maven
-----

Launch this command to download all needed files and create the `.jar` package (needed if whe want to launch it via the remote executor):

        $ mvn clean package

Note that if you want to use the remote executor with the Bulk iteration, you will need to replace this class `eu.stratosphere.quickstart.WorksetConnectedComponents` by `eu.stratosphere.quickstart.WorksetConnectedComponentsBulkIteration` in the `pom.xml` file.


Delta
-----

Simply launch `WorksetConnectedComponents` class with these arguments:

        1  file:///file/to/vertices.txt  file:///file/to/edges.txt  file:///file/to/output.txt  15

It will produce an `output.txt` file with the results and informations about the accumulators will be printed in the terminal (stdout).



Bulk
----

Simply launch `WorksetConnectedComponentsBulkIterator` class with these arguments:

        1  file:///file/to/vertices.txt  file:///file/to/edges.txt  file:///file/to/output.txt  15

It will produce an `output.txt` file with the results and informations about the accumulators will be printed in the terminal (stdout).


Remote Executor
---------------

To launch the remote executor instead of the local one, simply replace:

        JobExecutionResult jer = LocalExecutor.execute(plan);
        for (Entry<String, Object> o : jer.getAllAccumulatorResults().entrySet()) {
            System.out.println(o.getKey() + " -- " + o.getValue());
        }

by:

        PlanExecutor ex = new RemoteExecutor("localhost", 6123,
            "target/quickstart-java-0.1.jar");
        ex.executePlan(plan);

in the main class.


Results
-------

Output files are available in the `results` dir.
