import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;


public class ConnectedComponentsPreProcessing {

	private final static Charset ENCODING = StandardCharsets.UTF_8;

	public static String getDescription() {
		return "Parameters: <input>";
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println(getDescription());
			System.exit(1);
		}

		BufferedReader input = null;
		String outputVerticesFile = (args.length > 1 ? args[1] : "vertices.txt");
		BufferedWriter outputVertices = null;
		String outputEdgesFile = (args.length > 2 ? args[2] : "edges.txt");
		BufferedWriter outputEdges = null;
		try {
			input = Files.newBufferedReader(Paths.get(args[0]), ENCODING);
			outputVertices = Files.newBufferedWriter(
					Paths.get(outputVerticesFile), ENCODING);
			outputEdges = Files.newBufferedWriter(Paths.get(outputEdgesFile),
					ENCODING);
		} catch (FileNotFoundException e) {
			System.err.println("Boulet, le fichier n'existe pas...");
			System.exit(1);
		} catch (UnsupportedEncodingException e) {
			System.err.println("Quoi, tu n'utilises pas un vrai OS???");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Ça buggg... on ne supporte pas Windaube nous!");
			System.exit(1);
		}

		HashMap<Integer, Object> map = new HashMap<Integer, Object>();
		String line;
		int nodeFrom, nodeTo;
		int sep;
		try {
			while ((line = input.readLine()) != null) {
				if (line.startsWith("#"))
					continue;
				sep = line.indexOf('\t');
				nodeFrom = Integer.parseInt(line.substring(0, sep));
				nodeTo = Integer.parseInt(line.substring(sep + 1));

				map.put(nodeFrom, null);
				map.put(nodeTo, null);

				outputEdges.write(line + "\n" + nodeTo + "\t" + nodeFrom + "\n");
			}

			for (int node : map.keySet()) {
				outputVertices.write(node + "\n");
			}

			input.close();
			outputEdges.close();
			outputVertices.close();
		} catch (IOException e) {
			System.err.println("Franchement, pense à utiliser Linux...");
			System.exit(1);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("C'est quoi ce fichier pourri?");
			System.exit(1);
		} catch (NumberFormatException e) {
			System.out.println("Depuis quand on met des lettres dans un ID?");
			System.exit(1);
		}
	}
}
