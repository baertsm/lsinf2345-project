#!/bin/bash

PID_FILE="pidstat.log"
IO_FILE="iostat.log"
PS_FILE="ps.log"
CPU_FILE="cpu.log"
IF_FILE="if.log"

PREV_TOTAL=0
PREV_IDLE=0

cpu() {
	CPU=(`sed -n 's/^cpu\s//p' /proc/stat`)
	IDLE=${CPU[3]} # Just the idle CPU time.

	# Calculate the total CPU time.
	TOTAL=0
	for VALUE in "${CPU[@]}"; do
		let "TOTAL=$TOTAL+$VALUE"
	done

	# Calculate the CPU usage since we last checked.
	let "DIFF_IDLE=$IDLE-$PREV_IDLE"
	let "DIFF_TOTAL=$TOTAL-$PREV_TOTAL"
	let "DIFF_USAGE=(1000*($DIFF_TOTAL-$DIFF_IDLE)/$DIFF_TOTAL+5)/10"
	# echo -en "\rCPU: $DIFF_USAGE%  \b\b"
	uptime >> $CPU_FILE
	echo "CPU $DIFF_USAGE" >> $CPU_FILE

	# Remember the total and idle CPU times for the next check.
	PREV_TOTAL="$TOTAL"
	PREV_IDLE="$IDLE"
}

while true; do
	# CPU
	cpu

	# if
	ifconfig >> $IF_FILE

	# PS
	ps aux >> $PS_FILE

	# IO
	uptime >> $IO_FILE
	iostat >> $IO_FILE

	# PID
	pidstat >> $PID_FILE
	sleep 1
done
